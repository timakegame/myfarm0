using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StructureType
{
    road,
    building,
    wheat,
    melon,
    corn,
    milk,
    apple
}

public class Structure : MonoBehaviour
{
    public StructureType structureType;

    public bool fuctional = false; //for farm, it can auto-grow or not

    public string StructureName;

    public int hp = 1;

    public int costToBuild;
}
