using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Office : MonoBehaviour
{
    public int money;

    public int wheat;

    public int melon;

    public int corn;

    public int milk;

    public int apple;

    public int dailyCostWages;

    public List<Structure> structures = new List<Structure>();

    public List<Staff> staff = new List<Staff>();

    public static Office instance;

    [SerializeField] private int _availStaff;
    public int AvailStaff {get { return _availStaff; } set { _availStaff = value; }}

    public GameObject staffParent;

    public GameObject spawnPosition; // public Transform spawnPosition ของเดิม
    public GameObject rallyPosition; // public Transform spawnPosition ของเดิม

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void AddBuilding(Structure s)
    {
        structures.Add(s);
    }

    public void RemoveBuilding(Structure s)
    {
        structures.Remove(s);
        Destroy(s.gameObject);
    }

    public bool ToHireStaff(GameObject staffObj)
    {
        if (money <= 0)
        {
            return false;
        }

        staffObj.transform.parent = staffParent.transform;

        Staff s = staffObj.GetComponent<Staff>();
        //Debug.Log(s);
        s.Hired = true; // Hire
        s.ChangeCharSkin(); // Show 3D model
        s.SetToWalk(rallyPosition.transform.position);

        money -= s.dailyWage;
        AddStaff(s);

        UI.instance.UpdateResourceUI();

        return true;
    }

    public void AddStaff(Staff s)
    {
        staff.Add(s);
        dailyCostWages += s.dailyWage;
    }

   

    

    public void SendStaff(GameObject target)
    {
        Farm f = target.GetComponent<Farm>();
        int staffNeed = f.MaxStaffNum - f.WorkingStaff.Count;

        if (staffNeed <= 0)
        {
            return;
        }

        UpdateAvailStaff();

        if (staffNeed > _availStaff)
        {
            staffNeed = _availStaff;
        }

        int n = 0;

        for (int i = 0; i < staff.Count; i++)
        {
            if (staff[i].TargetStructure == null)
            {
                Staff s = staff[i].GetComponent<Staff>();

                staff[i].TargetStructure = target;
                staff[i].SetToWalk(target.transform.position);
                f.AddStaffToFarm(s);
                n++;
            }

            if (n >= staffNeed)
            {
            break;
            }
        }

        UpdateAvailStaff();   

    }

    public void UpdateAvailStaff()
    {
        _availStaff = 0;

        foreach (Staff s in staff)
        {
            if (s.TargetStructure == null) //there is no job to do
            {
                _availStaff++;
            }
        }
    }
}

