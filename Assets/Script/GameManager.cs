using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private int day = 0;

    public int Day
    {
        get
        {
            return day;
        }
        set
        {
            day = value;
        }
    }

    public static GameManager instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}
