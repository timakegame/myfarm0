using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] AudioSource[] BGM;
    [SerializeField] AudioSource[] SFX;

    public static AudioManager instance;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StopBGM()
    {
        for(int i = 0; i < BGM.Length; i++)
        {
            BGM[i].Stop();
        }
    }

    public void PlayBGM(int i)
    {
        StopBGM();
        if (BGM[i].isPlaying == false)
        {
            BGM[i].Stop();
        }
    }
}
