using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManager : MonoBehaviour
{
    [SerializeField] private List<Staff> staffs = new List<Staff>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Idle()
    {
        foreach(Staff s in staffs)
        {
            s.SetState(UnitState.Idle);
        }
    }

    public void Walk()
    {
        foreach(Staff s in staffs)
        {
            s.SetState(UnitState.Walking);
        }
    }

    public void Water()
    {
        foreach(Staff s in staffs)
        {
            s.SetState(UnitState.Watering);
        }
    }

    public void Plow()
    {
        foreach(Staff s in staffs)
        {
            s.SetState(UnitState.Plowing);
        }
    }

    public void Sow()
    {
        foreach(Staff s in staffs)
        {
            s.SetState(UnitState.Sowing);
        }
    }

    public void Harvesting()
    {
        foreach(Staff s in staffs)
        {
            s.SetState(UnitState.Harvesting);
        }
    }
}
