using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text moneyText;

    public Text staffText;

    public Text wheatText;

    public Text melonText;

    public Text cornText;

    public Text milkText;

    public Text appleText;

    public Text dayText;

    public static UI instance;

    public GameObject LaborMarketPanel;

    public GameObject farmPanel;

    [SerializeField] private Text _farmNameText;
    public Text FarmNameText { get { return _farmNameText; } set { _farmNameText = value; } }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        UpdateResourceUI();
        UpdateTimeUI();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void UpdateResourceUI()
    {
        moneyText.text = Office.instance.money.ToString();
        staffText.text = Office.instance.staff.Count.ToString();
        wheatText.text = Office.instance.wheat.ToString();
        melonText.text = Office.instance.melon.ToString();
        cornText.text = Office.instance.corn.ToString();
        milkText.text = Office.instance.milk.ToString();
        appleText.text = Office.instance.apple.ToString();
    }

    public void UpdateTimeUI()
    {
        dayText.text = GameManager.instance.Day.ToString();
    }

    public void ToggleLaborPanel()
    {
        if(!LaborMarketPanel.activeInHierarchy)
        {
            LaborMarketPanel.SetActive(true);
        }else{
            LaborMarketPanel.SetActive(false);
        }
    }

      public void ToggleFarmPanel()
    {
        if(!farmPanel.activeInHierarchy)
        {
           farmPanel.SetActive(true);
        }else{
            farmPanel.SetActive(false);
        }
    }
}
